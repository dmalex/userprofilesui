//
//  UserProfile309App.swift
//  UserProfile309
//  Created by brfsu on 31.01.2024.
//
import SwiftUI

@main
struct UserProfile309App: App {
    var body: some Scene {
        WindowGroup {
            UserProfileCV()
        }
    }
}
