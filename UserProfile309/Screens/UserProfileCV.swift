//
//  ContentView.swift
//  UserProfile309
//  Created by brfsu on 31.01.2024.
//
import SwiftUI

struct UserProfileCV: View
{
    @StateObject var viewModel = UserModel()
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    HStack {
                        ChangableAvatarView(viewModel: viewModel)
                        VStack {
                            TextField("First name", text: $viewModel.firstName)
                            TextField("Last name", text: $viewModel.lastName)
                            TextField("Surname", text: $viewModel.surname)
                        }.padding(.leading)
                    }
                }
                
                Section {
                    TextField("Email", text: $viewModel.email)
                    TextField("Telegramm", text: $viewModel.tg)
                    TextField("Phone", text: $viewModel.tel)
                    TextField("Nickname", text: $viewModel.nickname)
                }

                Section {
                    Button(action: {
                        viewModel.saveInUserDefaults()
                    }, label: {
                        Text("Save")
                    })
                    Button(action: {
                        restore(viewModel: viewModel)
                    }, label: {
                        Text("Cancel")
                    })
                }

                
            }
            .navigationTitle("Profile")
        }
        .cornerRadius(30)
        .background(.white)
        .padding(5)
        .onAppear {
            restore(viewModel: viewModel)
        }

    }
    
    
    @MainActor
    func restore(viewModel: UserModel) {
        // print("restore")
        let data = UserDefaults.standard.data(forKey: "Avatar") ?? UIImage(named: "Warning")!.jpegData(compressionQuality: 1)!
        let image = UIImage(data: data)!
        viewModel.setImageStateSuccess(image: Image(uiImage: image))
        
        for key in viewModel.keyValues {
            switch key {
            case "FirstName":
                viewModel.firstName = UserDefaults.standard.string(forKey: key) ?? ""
            case "LastName":
                viewModel.lastName = UserDefaults.standard.string(forKey: key) ?? ""
            case "Surname":
                viewModel.surname = UserDefaults.standard.string(forKey: key) ?? ""
            case "Email":
                viewModel.email = UserDefaults.standard.string(forKey: key) ?? ""
            case "TG":
                viewModel.tg = UserDefaults.standard.string(forKey: key) ?? ""
            case "Tel":
                viewModel.tel = UserDefaults.standard.string(forKey: key) ?? ""
            case "Nickname":
                viewModel.nickname = UserDefaults.standard.string(forKey: key) ?? ""
            default:
                print("Unknown value")
            }
        }
    }
}
