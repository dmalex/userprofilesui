//
//  ProfileImage.swift
//  UserProfile309
//  Created by brfsu on 31.01.2024.
//
import SwiftUI
import PhotosUI

struct ProfileImage: View
{
    let imageState: UserModel.ImageState

    var body: some View {
        switch imageState {
        case .success(let image):
            image.resizable()
        case .loading: // (let progress):
            ProgressView()
        case .empty:
            Image("Avatar")
                .resizable()
                .scaledToFit()
                .font(.system(size: 40))
                .foregroundColor(.white)
        case .failure: // (let error):
            Image("Warning")
                .font(.system(size: 40))
                .foregroundColor(.white)
        }
    }
}
