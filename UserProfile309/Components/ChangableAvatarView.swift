//
//  ChangableAvatarView .swift
//  UserProfile309
//  Created by brfsu on 31.01.2024.
//
import SwiftUI
import PhotosUI

struct ChangableAvatarView: View
{
    @ObservedObject var viewModel: UserModel

    var body: some View {
        ProfileImage(imageState: viewModel.imageState)
            .scaledToFill()
            .clipShape(Circle())
            .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: 100)
            .background {
                Circle().fill(
                    Color(.red))
            }
            .overlay(alignment: .center) {
                PhotosPicker(selection: $viewModel.imageSelection, matching: .images, photoLibrary: .shared()) {
                    Circle()
                        .opacity(0.1)
                }
                .buttonStyle(.borderless)
            }
    }
}
